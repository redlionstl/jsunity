function Setup(featureAPI, implementation){
	
	var setupCompleted = false;
	var isAsync = false;

	this.reset = function(){
		setupCompleted = false;
		isAsync = false;
	};
	
	var self = this;
	
	this.invoke = (function(){
		var args = Array.prototype.slice.apply(arguments, [0]);
		try{
			implementation.apply(self, args);
			if(!isAsync && !setupCompleted){
				setupCompleted = true;
				featureAPI.setupComplete(self, []);
			}

		}catch(e){
			if(e.name == 'AssertionError'){
				featureAPI.getFormatter().log("Error caught calling setup: Expected {" + e.expected + "} got {" + e.actual +"}");
			}else{
				featureAPI.getFormatter().log("Error caught calling setup: " + e);
			}
			featureAPI.setupFailed(self);
		}
	});
	
	this.fail = function(){
		if(!setupCompleted){
			setupCompleted = true;
			featureAPI.setupComplete(self, []);
		}
	};
	
	this.done = function(){
		if(!setupCompleted){
			setupCompleted = true;
			featureAPI.setupComplete(self, Array.prototype.slice.apply(arguments,[0]));
		}
	};
	
	this.makeCallback = function(userCallback){
		isAsync = true;
		var args = Array.prototype.slice.apply(arguments, [1]);
		return function(){
			var innerArgs = Array.prototype.slice.apply(arguments,[0]);
			try{
				userCallback.apply(self, args.concat(innerArgs));
				if(!setupCompleted){
					setupCompleted = true;
					featureAPI.setupComplete(self, []);
				}
			}catch(e){
				if(e.name == 'AssertionError'){
					featureAPI.getFormatter().log("Error caught calling setup: Expected {" + e.expected + "} got {" + e.actual +"}");
				}else{
					featureAPI.getFormatter().log("Error caught calling setup: " + e);
				}
				featureAPI.setupFailed(self);
			}
		};
	};
	
};

module.exports = Setup;
