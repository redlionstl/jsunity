var path = require("path");
var Setup = require(path.join(__dirname,"Setup"));
var Test = require(path.join(__dirname,"Test"));
var Formatter = require(path.join(__dirname,"formatter"));
var util = require("util");
var events = require("events");

var rootFeature = null;

process.on('SIGINT', function(){
	console.error("Testing ended by user.");
	if(rootFeature){
		rootFeature.hardExit();
	}else{
		console.error("No root feature set.");
	}
});

function Feature(featureAPI, description, formatter){

	// set the first feature defined.
	if(!featureAPI && !rootFeature) rootFeature = this;
	
	
	var featureEvents = new events.EventEmitter;

	description = description?description:'JsUnity';
	
	formatter = formatter || Formatter;


	var features = [];
	var setups = [];
	var tests = [];
	var tearDowns = [];

	var init = {
		features: [],
		setups: [],
		tests: [],
		tearDowns: []
	};



	var lastFeature = false;
	//var testsRunning = [];
	
	var featureParams = [];
	var setupOutput = []; 
	var featureList = false;	
	var setupList = false;

	var featureOnDone = function(){};

	function reset(){
		featureEvents.removeAllListeners("featureComplete");
		featureEvents.removeAllListeners("testComplete");
		
		featureEvents.on("featureComplete", untilAllFeaturesAreDone);
		featureEvents.on("testComplete", untilAllTestsAreDone);
		
		features = init.features.slice(0);
		setups = init.setups.slice(0);
		tests = init.tests.slice(0);
		tearDowns = init.tearDowns.slice(0);

		lastFeature = false;
		testsRunning = [];
		testList = [];
		featureParams = [];
		setupOutput = [];
		featureList = false;
		setupList = false;
		featureOnDone = function(){};
		
		setups.forEach(function(setup){
			setup.reset();
		});
		tests.forEach(function(test){
			test.reset();
		});
	}

	var self = this;
	
	var localAPI = {
			setupFailed: function(setup){
				features = [];
				formatter.log("Terminating setups");
				setupList = [];
				formatter.log("Terminating tests");
				failTests();
				startTearDowns();
			},
			setupComplete: function(setup, output){
				setupOutput = setupOutput.concat(output);
				var position = setups.indexOf(setup);
				if(position != -1){
					setups = setups.slice(0, position).concat(setups.slice(position +1));
					if(setups.length == 0){
						// setups have all completed
						startTests();
					}else{
						runSetups();
					}
				}else{
					throw new Error("Setup completed more than once.", description);
				}
			},
			featureComplete: function(feature){
				if(lastFeature == feature) lastFeature = false;
				var position = features.indexOf(feature);
				if( position != -1){
					features = features.slice(0, position).concat( features.slice(position + 1));
				}else{
					throw Error("Feature was not in feature when removed.");
				};
				featureEvents.emit('featureComplete', feature);
			},
			getParent: function(){
				return self;
			},
			testComplete: function(test){
				featureEvents.emit('testComplete', test);
			},
			getFormatter: function(){
				return formatter;
			}
	};
	
	
	function startTearDowns(){
		tearDowns.forEach(function(tearDown){
			try{
				tearDown.apply(self, setupOutput);
			}catch(e){
				formatter.log("Error running tearDown: " + description);
			}
		});
		self.done();
	}
	
	function startSetups(){
		if(setups.length){
			setupList = setups.slice(0);
			runSetups();
		}else{
			startTests();
		}
	}

	function failTests(){
		// stop running testList,  it probably should not be running yet
		
		testList = [];
		tests.forEach(function(test){
			formatter.test(test.getDescription(), 'error', "Error in setup.");
		});
	}
	// run setups sequentially
	function runSetups(){
		if(setupList.length){
			var currentSetup = setupList.shift();
			process.nextTick(function () {
				currentSetup.invoke.apply(currentSetup, featureParams);
			});
		}
	}

	
	
	function startTests(){
		testList = tests.slice(0);
		runTests();
	}
	
	function runTests(){
		var test = testList.shift();
		if (test){
			test.invoke.apply(test, setupOutput );
		} else {
			startFeatures();
		}
	};
	
	
	function startFeatures(){
		if(features.length){
			featureList = features.slice(0);
			runFeatures();
		}else{
			startTearDowns();
		}
	}
	
	function runFeatures(){
		var feature = featureList.shift();
		lastFeature = feature;
		if(feature){
			feature.run(null, setupOutput);
		};
	}
	
	function untilAllFeaturesAreDone(){
		if(features.length == 0){
			featureEvents.removeListener('featureComplete', untilAllFeaturesAreDone);
			startTearDowns();
		}else{
			runFeatures();
		}
	};


	function untilAllTestsAreDone(feature){
		// A test was removed.
		// See if it was the last test...
		if(testList.length == 0){
			// stop listening for tests to stop.  They all have.
			featureEvents.removeListener('testComplete', untilAllTestsAreDone);
			// start running the features in sequence.
			startFeatures(); 
		} else {
			runTests();
		}
		
	}
	
	this.getDescription = function(){
		return description;
	};
	
	this.setup = function(implementation){
		init.setups.push( new Setup(localAPI, implementation));
		return self;
	};

	this.feature = function(description){
		var child =  new Feature(localAPI, description, formatter);
		init.features.push(child);
		return child;
	};
		
	this.test = function(description, implementation){
		init.tests.push(new Test(localAPI, description, implementation));
		return self;
	};

	this.tearDown = function(implementation){
		if(implementation){
			init.tearDowns.push(implementation);
		};
		return featureAPI.getParent();
	};
	
	this.done = function(){
		if(formatter){
			formatter.decreaseIndent();
		}
		if(featureAPI){
			featureAPI.featureComplete(self);
		}else{
			if(formatter) formatter.endReport();
			featureOnDone();
		}
	};
	
	this.run = function(onDone, params){
		params = params?params:[];

		reset(); // reset base values so we can run multiple times.

		if(onDone && typeof(onDone) == 'function'){
			featureOnDone = onDone ;
		}
		
		// if the parent feature has passed any parameters to this feature, they become
		// arguments to the setup functions.
		featureParams = featureParams.concat(params);
		
		formatter.feature(description);
		
		process.nextTick(function(){
			startSetups();		
		});
	};
	
	this.hardExit = function(){
		if(lastFeature){
			if(featureAPI) formatter.log("Last Feature started:" + description);
			lastFeature.hardExit();
		}else{
			if(testsRunning){
				formatter.log("Tests in progress: ");
				formatter.increaseIndent();
				testsRunning.forEach(function(test){
					formatter.log(test.getDescription());
				});
			}
		}
		process.exit(-1);
	};
};

module.exports = Feature;
