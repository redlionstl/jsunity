
function Test(featureAPI, description, implementation){

	var self = this;

	var message = 'Test not run.';
	var success = null;
	var hasCompleted = false;
	var isAsync = false;

	this.reset = function(){
		message = 'Test not run';
		success = null;
		hasCompleted = false;
		isAsync = false;
	};
	
	this.getDescription = function(){
		return description;
	};
	
	this.getMessage = function(){
		return message;
	};
	
	this.getSuccess = function(){
		return success;
	};
	
	this.fail = function(inMessage){
		if(!hasCompleted){
			hasCompleted = true;	
			inMessage = inMessage || "No failure message supplied";
			message = inMessage;
			success = false;
			featureAPI.getFormatter().test(description, 'fail', message);
			featureAPI.testComplete(self);	
		}else{
			console.error("WTF");
		}
	};
	this.error = function(e){
		if(!hasCompleted){
			hasCompleted = true;
			message = e.message || e || "";
			success = false;
			featureAPI.getFormatter().test(description, 'error', message);
			featureAPI.testComplete(self);
		}else{
			console.error("WTF");
		};
	};
	
	this.pass = function(){
		if(!hasCompleted){
			hasCompleted = true;

			message = null;
			success = true;
			featureAPI.getFormatter().test(description, 'pass', '');
			featureAPI.testComplete(self);
		};
	};
	
	this.invoke = function(){
		var args = Array.prototype.slice.apply(arguments,[0]);
		try {
			implementation.apply(self, args);
			if(!isAsync){
				self.pass();
			}
		} catch (e) {
			if(e.name == 'AssertionError'){
				self.fail("Expected {" + e.expected + "} got {"+ e.actual + "}" );	
			}else{
				self.error(e);
			}
		}
	};
	
	this.makeCallback = function(userCallback){
		isAsync = true;
		var args = Array.prototype.slice.apply(arguments, [1]);
		return function(){
			var innerArgs = Array.prototype.slice.apply(arguments,[0]);
			try{
				userCallback.apply(self, args.concat(innerArgs));
				self.pass();
			}catch(e){
				if(e.name == 'AssertionError'){
					self.fail("Expected {" + e.expected + "} got {"+ e.actual + "}" );	
				}else{
					//featureAPI.getFormatter().test(description, 'error', e.message);
					self.error(e);
				}
			}
		};
	};
};


module.exports = Test;
