var colors = require('colors');


function Formatter () {
	var lineSeparator = '\n';
	var lineIndent = '    ';
	var indentLevel = 0;
	var features = 0;
	var passes = 0;
	var failures = 0;
	var errors = 0;
	
	
	var self = this;
	
	
	function tests () {
		return passes + failures + errors;
	};
	
	
	function indent () {
		var indent = '';
		for (var i=0; i<indentLevel; i++) {
			indent += lineIndent;
		}
		return indent;
	};
	
	
	function error (testDescription, errorMessage) {
		errors++;
		console.error(indent() + testDescription.yellow);
		self.increaseIndent();
		console.error(indent() + errorMessage.yellow);
	};
	
	
	function success (testDescription) {
		passes++;
		console.log(indent() + testDescription.green);
	};
	
	
	function failure (testDescription, failureMessage) {
		failures++;
		console.log(indent() + testDescription.red);
		self.increaseIndent();
		console.error(indent() + failureMessage.red);
		self.decreaseIndent();
	};
	
	
	this.log = function (string) {
		console.log(indent() + string);
	};
	
	
	this.increaseIndent = function () {
		indentLevel++;
	};
	
	
	this.decreaseIndent = function () {
		if (indentLevel >= 0) {
			indentLevel--;
		} else {
			throw new Error ('should not be decreasing indent level below 0');
		}
	};
	
	
	this.feature = function (featureName) {
		if (typeof(featureName) !== 'string') {
			throw new error ('`featureName` must be a string');
		}
		if (indentLevel == 0) {
			this.log(lineSeparator);
		} else {
			this.log('');
		}
		features++;
		this.log(featureName);
		self.increaseIndent();
	};
	
	
	this.test = function (testDescription, result, message) {
		switch (result) {
			case 'pass':
						success(testDescription);
						break;
			case 'fail':
						failure(testDescription, message);
						break;
			case 'error':
						error(testDescription, message);
						break;
			default:
						throw new Error ('no such test result `' + result + '`');
		}
	};
	
	
	this.endReport = function () {
		console.log(lineSeparator);
		console.log(
				features + ' features, ' +
				tests()  + ' tests\n' +
				(passes   + ' passes, ').green +
				(failures + ' failures, ').red +
				(errors   + ' errors\n').yellow
		);
		indentLevel = 0;
		features = 0;
		passes = 0;
		failures = 0;
		errors = 0;
	};
	
	
};


module.exports = new Formatter();
