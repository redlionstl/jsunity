var path=require("path");
var jsUnity = require(path.join(__dirname,"..","lib","jsunity"));
var assert = require("assert");
var path = require("path");

var jsunity = new jsUnity();
jsunity
.feature("Flarg")
	.setup(function(){
		var pathExistsCallback = this.makeCallback(function(exists){
			var result2 = 4;
			var result3 = 12;
			this.done(exists, result2, result3);
		});
		path.exists('/home/paul/jank', pathExistsCallback);
	})
	.feature("JsUnity Module")
		.setup(function(){
			var pathExistsCallback = this.makeCallback(function(exists){
				var result2 = 4;
				var result3 = 12;
				this.done(exists, result2, result3);
			});
			path.exists('/home/paul/jank', pathExistsCallback);
		})
		.test('The module exports a constructor', function(){
			assert.equal(typeof(jsUnity), 'function');
		})
		.test("the constructor exported is a Feature constructor", function(){
			assert.equal(jsUnity.name, 'Feature');
		})
	.tearDown()
	.feature('The Feature object')
		.setup(function(){
			this.done(new jsUnity(null, "TestableFeature"));
		})
		.setup(function(){
			var callback = this.makeCallback(function(){
				
			});
			callback();
		})
		.test("it can be used to create a new feature", function(){
			var callback = this.makeCallback(function(first, second){
				assert.equal(first, "Woot!");
				assert.equal(second, "SPANK");
			});
			setTimeout(function(){
				callback("Woot!", "spank");
			}, 4000);
		})
		.test("flarkity flark flark", function(){
			var callback = this.makeCallback(function(first, second){
				assert.equal(first, "Woot!");
				assert.equal(second, "spank");
			});
			setTimeout(function(){
				callback("Woot!", "spank");
			}, 2000);
		})
	.tearDown();

module.exports = jsunity;
